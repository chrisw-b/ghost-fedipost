declare module 'uppercamelcase' {
  const convert: (...paths: string[]) => string;
  export default convert;
}
